<?php

/**
 * @file
 * Module file for D01 toolbar.
 */

use Drupal\d01_drupal_toolbar\Form\D01DrupalToolbarSettingsForm;
use Drupal\Core\Url;

/**
 * Implements hook_toolbar().
 */
function d01_drupal_toolbar_toolbar() {
  $items = [];

  // Make sure we don't cache primary tasks.
  $items['d01_drupal_toolbar_primary_tasks'] = [
    '#cache' => [
      'max-age' => 0,
    ],
  ];

  // Make sure we don't cache secondary tasks.
  $items['d01_drupal_toolbar_secondary_tasks'] = [
    '#cache' => [
      'max-age' => 0,
    ],
  ];

  // Check if local tasks functionality is activated.
  $config = \Drupal::config(D01DrupalToolbarSettingsForm::CONFIG_NAME);
  if (!$config->get('local_tasks_active')) {
    return $items;
  }

  // Check if local tasks functionality is activated for admin routes.
  $route = \Drupal::routeMatch()->getRouteObject();
  $is_admin = \Drupal::service('router.admin_context')->isAdminRoute($route);
  if (!$config->get('local_tasks_show_on_admin_routes') && $is_admin) {
    return $items;
  }

  // Get all local tasks.
  $primary_local_tasks = d01_drupal_toolbar_toolbar_primary_local_tasks();
  $secondary_local_tasks = d01_drupal_toolbar_toolbar_secondary_local_tasks();

  $items['d01_drupal_toolbar_primary_tasks'] = array_merge($items['d01_drupal_toolbar_primary_tasks'], $primary_local_tasks);
  $items['d01_drupal_toolbar_secondary_tasks'] = array_merge($items['d01_drupal_toolbar_secondary_tasks'], $secondary_local_tasks);

  return $items;
}

/**
 * Add primary local tasks to toolbar.
 *
 * @return array
 *   A toolbar_item render array.
 */
function d01_drupal_toolbar_toolbar_primary_local_tasks() {
  $item = [];

  // Get all local tasks.
  $local_task_service = \Drupal::service('d01_drupal_toolbar.local_tasks');
  $primary_tasks = $local_task_service->getPrimaryToolbarLinks();

  if (empty($primary_tasks)) {
    return $item;
  }

  // Build toolbar links.
  $item = [
    '#type' => 'toolbar_item',
    '#weight' => 150,
    '#attached' => [
      'library' => [
        'd01_drupal_toolbar/toolbar',
      ],
    ],
    'tab' => [
      '#type' => 'link',
      '#title' => t('Primary Links'),
      '#url' => Url::fromRoute('system.admin'),
      '#attributes' => [
        'title' => t('Primary Links'),
        'data-drupal-subtrees' => TRUE,
        'class' => [
          'toolbar-icon',
          'toolbar-icon-links',
        ],
      ],
      '#options' => [
        'attributes' => [
          'title' => t('Primary Links'),
        ],
      ],
    ],
    'tray' => [
      '#heading' => t('Primary Links'),
      'local_tasks' => [
        '#theme' => 'item_list',
        '#items' => $primary_tasks,
        '#attributes' => [
          'class' => [
            'toolbar-menu',
            'toolbar-links-menu',
            'toolbar-links-menu-main',
          ],
        ],
      ],
    ],
  ];

  return $item;
}

/**
 * Add secondary local tasks to toolbar.
 *
 * @return array
 *   A toolbar_item render array.
 */
function d01_drupal_toolbar_toolbar_secondary_local_tasks() {
  $item = [];

  // Get all local tasks.
  $local_task_service = \Drupal::service('d01_drupal_toolbar.local_tasks');
  $secondary_tasks = $local_task_service->getSecondaryToolbarLinks();

  if (empty($secondary_tasks)) {
    return $item;
  }

  // Build toolbar links.
  $item = [
    '#type' => 'toolbar_item',
    '#weight' => 150,
    '#attached' => [
      'library' => [
        'd01_drupal_toolbar/toolbar',
      ],
    ],
    'tab' => [
      '#type' => 'link',
      '#title' => t('Secondary Links'),
      '#url' => Url::fromRoute('system.admin'),
      '#attributes' => [
        'title' => t('Secondary Links'),
        'data-drupal-subtrees' => TRUE,
        'class' => [
          'toolbar-icon',
          'toolbar-icon-links',
        ],
      ],
      '#options' => [
        'attributes' => [
          'title' => t('Secondary Links'),
        ],
      ],
    ],
    'tray' => [
      '#heading' => t('Secondary Links'),
      'local_sub_tasks' => [
        '#theme' => 'item_list',
        '#items' => $secondary_tasks,
        '#attributes' => [
          'class' => [
            'toolbar-menu',
            'toolbar-links-menu',
            'toolbar-links-menu-secondary',
          ],
        ],
      ],
    ],
  ];

  return $item;
}