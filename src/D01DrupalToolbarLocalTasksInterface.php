<?php

namespace Drupal\d01_drupal_toolbar;

use Drupal\Core\Access\AccessManagerInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\Core\Menu\LocalTaskManager;
use Drupal\Core\Session\AccountInterface;

/**
 * Interface D01DrupalToolbarLocalTasksInterface.
 *
 * @package Drupal\d01_drupal_toolbar
 */
interface D01DrupalToolbarLocalTasksInterface {

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Routing\CurrentRouteMatch $current_route_match
   *   The current route match service.
   * @param \Drupal\Core\Menu\LocalTaskManager $local_task_manager
   *   The local tasks manager.
   * @param \Drupal\Core\Access\AccessManagerInterface $access_manager
   *   The access manager.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current user.
   */
  public function __construct(CurrentRouteMatch $current_route_match, LocalTaskManager $local_task_manager, AccessManagerInterface $access_manager, AccountInterface $account);

  /**
   * Get the primary local tasks.
   *
   * @return \Drupal\core\Link[]
   *   An array of Link objects.
   */
  public function getPrimaryToolbarLinks();

  /**
   * Get the secondary local tasks.
   *
   * @return \Drupal\core\Link[]
   *   An array of Link objects.
   */
  public function getSecondaryToolbarLinks();

}
