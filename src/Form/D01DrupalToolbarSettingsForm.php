<?php

namespace Drupal\d01_drupal_toolbar\Form;

use Drupal\Core\Form\ConfigFormBase;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\WebformElementInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\webform\Plugin\WebformElementManager;
use Drupal\Core\Form\FormState;

/**
 * Class D01DrupalToolbarSettingsForm.
 *
 * @package Drupal\d01_drupal_toolbar\Form
 */
class D01DrupalToolbarSettingsForm extends ConfigFormBase {

  const CONFIG_NAME = 'd01_drupal_toolbar.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'd01_drupal_toolbar_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      D01DrupalToolbarSettingsForm::CONFIG_NAME,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, Request $request = NULL) {
    $config = $this->config(D01DrupalToolbarSettingsForm::CONFIG_NAME);

    $form['local_tasks'] = [
      '#type' => 'fieldset',
      '#title' => t('Local tasks'),
      '#description' => t('Setting for the local tasks menu in the D01 toolbar.'),
    ];

    $form['local_tasks']['local_tasks_active'] = [
      '#type' => 'checkbox',
      '#title' => t('Show local tasks in toolbar'),
      '#default_value' => $config->get('local_tasks_active'),
    ];

    $form['local_tasks']['local_tasks_show_on_admin_routes'] = [
      '#type' => 'checkbox',
      '#title' => t('Show local tasks in toolbar on the admin routes'),
      '#default_value' => $config->get('local_tasks_show_on_admin_routes'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config(D01DrupalToolbarSettingsForm::CONFIG_NAME);
    $config->set('local_tasks_active', $form_state->getValue('local_tasks_active'));
    $config->set('local_tasks_show_on_admin_routes', $form_state->getValue('local_tasks_show_on_admin_routes'));
    $config->save();
    parent::submitForm($form, $form_state);
  }

}
