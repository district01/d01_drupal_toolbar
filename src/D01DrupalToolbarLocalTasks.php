<?php

namespace Drupal\d01_drupal_toolbar;

use Drupal\Core\Access\AccessManagerInterface;
use Drupal\Core\Link;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\Core\Menu\LocalTaskManager;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Component\Utility\SortArray;

/**
 * Class D01DrupalToolbarLocalTasks.
 *
 * @package Drupal\d01_drupal_toolbar
 */
class D01DrupalToolbarLocalTasks implements D01DrupalToolbarLocalTasksInterface {

  /**
   * The current route match service.
   *
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */
  protected $currentRouteMatch;

  /**
   * The local tasks manager.
   *
   * @var \Drupal\Core\Menu\LocalTaskManager
   */
  protected $localTaskManager;


  /**
   * The access manager.
   *
   * @var \Drupal\Core\Access\AccessManagerInterface
   */
  protected $accessManager;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $account;

  /**
   * {@inheritdoc}
   */
  public function __construct(CurrentRouteMatch $current_route_match, LocalTaskManager $local_task_manager, AccessManagerInterface $access_manager, AccountInterface $account) {
    $this->currentRouteMatch = $current_route_match;
    $this->localTaskManager = $local_task_manager;
    $this->accessManager = $access_manager;
    $this->account = $account;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_route_match'),
      $container->get('plugin.manager.menu.local_task'),
      $container->get('access_manager'),
      $container->get('current_user')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getPrimaryToolbarLinks() {
    // Get the current route.
    $current_route = $this->currentRouteMatch->getRouteName();

    // Get the local tasks for the current route.
    $local_tasks = $this->localTaskManager->getLocalTasks($current_route, 0);

    // Get links for local tasks.
    $links = $this->getToolbarLinks($local_tasks);

    return $links;
  }

  /**
   * {@inheritdoc}
   */
  public function getSecondaryToolbarLinks() {
    // Get the current route.
    $current_route = $this->currentRouteMatch->getRouteName();

    // Get the local tasks for the current route.
    $local_tasks = $this->localTaskManager->getLocalTasks($current_route, 1);

    // Get links for local tasks.
    $links = $this->getToolbarLinks($local_tasks);

    return $links;
  }

  /**
   * Get the link objects.
   *
   * @param array $local_tasks
   *   An array of local tasks.
   *
   * @return \Drupal\core\Link[]
   *   An array of Link objects.
   */
  public function getToolbarLinks(array $local_tasks) {
    $links = [];

    // Sort them by weight.
    uasort($local_tasks['tabs'], [SortArray::class, 'sortByWeightProperty']);

    foreach ($local_tasks['tabs'] as $key => $task) {
      $title = $task['#link']['title'];
      $url = $task['#link']['url'];

      // Only include tasks which current user is allowed to access.
      $has_access = $this->accessManager->checkNamedRoute($url->getRouteName(), $url->getRouteParameters(), $this->account);

      if ($has_access) {
        $links[] = Link::fromTextAndUrl($title, $url);
      }
    }

    return $links;
  }

}
